<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_lawfordrobinson' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mysql' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@qZfZqKG{(crpQ7J].r>X?Lt`UL2Xll>ig=X-e g:UdQ*o,Bm/:B^tq1u{!1`[%V' );
define( 'SECURE_AUTH_KEY',  '55|5xn_J3CS$ Mf:rZ4jJ|-W`d@RF5!!57e1Vb2`JdM0a;eCD02QnQ2ZkbqevoE%' );
define( 'LOGGED_IN_KEY',    '1){zN:CNX_`3]&4DXbqkegh^+Q87%CKCe+n&/QJ.oq=V7;4j9<pU</MSrX|45eka' );
define( 'NONCE_KEY',        'NcU~,iqX0{S/uJWlTbn@@0UE~RF]l}S53NC.;WVI1Kfjb@tK|O3wcGLmHSs+VT{F' );
define( 'AUTH_SALT',        'lY0bt_1vA`UqwAL]i;:WLmKn_[FTu&mS0Y*.N7cN.]X;&rK1c]%Y(o_%~6_4yhLV' );
define( 'SECURE_AUTH_SALT', '1{PxDyB%~dd4s+Du)Gv1/]Pp?I9d&~091{nH]2MoVA-]Av!,N5vkKyY:j8 Y`!o}' );
define( 'LOGGED_IN_SALT',   '=S*~z{kE&71MD<@Bd`}4sOxS7p9vN&!n@tk)DE@u_Ngbmz(enGe8(|}N@-)|Y(&.' );
define( 'NONCE_SALT',       'x|Nw]uIZ^A1E/af?6j,KTj8m8CCP]d4G@hRe6n<ag7@gguIH$Z7-}]A0pWMe{GIl' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
