<footer>
  <div class="container-fluid">
    <div class="footer-upper">
      <div class="row">
        <div class="col-md-4">
          <div class="footer-brand">
            <img src="<?php echo e(get_template_directory_uri().'/assets/images/brand.png'); ?>" alt="">
          </div>
        </div>
        <div class="col-md-8 text-right">
        	<?php echo wp_nav_menu( array( 'theme_location' => 'footer_navigation', 'container_class' => 'footer-menu' ) );; ?>

        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="row">
        <div class="col-md-6">
          <p class="copyright">© Copyright 2019. Lawford Robinson, All Rights Reserved</p>
        </div>
        <div class="col-md-6 text-right">
          <p>Site by Hello Again</p>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Optional JavaScript -->
<script>
  jQuery(document).ready(function(){
    jQuery(".event-slider").owlCarousel({
      // autoplay: true,
      // autoplayTimeout: 4000,
      dots: true,
      dotsData:true,
      loop: true,
      margin: 30,
      nav: true,
      items: 1,
      navText: ["<span class='icon icon-prev'>","<span class='icon icon-next'>"]

    });
  });
</script>