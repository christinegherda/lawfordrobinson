
    <?php if(have_rows('banner') ): ?>
      <?php while(have_rows('banner')): ?> <?php the_row() ?>
        <section class="section-banner" style="background-image: url('<?php echo e(get_sub_field('banner_image')); ?>')">
          <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div class="banner-content">
            <div class="container">
              <div class="banner-text">
                <h2><?php echo e(get_sub_field('banner_title')); ?></h2>
                <p><?php echo e(get_sub_field('banner_subtitle')); ?></p>
                <a type="button" href="<?php echo e(get_sub_field('banner_contact_link')); ?>" class="btn btn-white">Contact Us</a>
              </div>
              <div class="scroll-down">
                <p>Scroll Down</p>
                <span class="icon-scrolldown"></span>
              </div>
            </div>
          </div>
        </section>
      <?php endwhile; ?>
    <?php endif; ?>
    <?php if(have_rows('expertise') ): ?>
      <?php while(have_rows('expertise')): ?> <?php the_row() ?>
        <section class="section-expertise">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <h2 class="title-md"><?php echo e(get_sub_field('expertise_title')); ?></h2>
                <p class="text-default">
                  <?php echo e(get_sub_field('expertise_description')); ?>

                </p>
                <a href="<?php echo e(get_sub_field('learn_more_link')); ?>" class="text-link">Learn More</a>
              </div>
              <div class="col-md-6">

              <div class="venn-container">
                  <?php if(have_rows('venn_diagram')): ?>
                    <?php while(have_rows('venn_diagram')): ?> <?php the_row() ?>
                    <?php
                      $circleWrapper = "";
                      $circleText = "";
                      if (get_row_index() == 1) {
                        $circleWrapper = "venncirctop";
                        $circleText = "venntxttop";
                      } elseif (get_row_index() == 2) {
                        $circleWrapper = "venncirclft";
                        $circleText = "venntxtlft";
                      } elseif (get_row_index() == 3) {
                        $circleWrapper = "venncircrt";
                        $circleText = "venntxtrt";
                      } else {
                        $circleWrapper = "venncircbtm";
                        $circleText = "venntxtbtm";
                      }
                    ?>
                    <div class="<?php echo e($circleWrapper); ?>">
                    <a class="<?php echo e($circleText); ?>" href="#"><?php echo get_sub_field('venn_item', true); ?></a>
                    </div>
                    <?php endwhile; ?>
                  <?php endif; ?>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>

              </div>


              </div>
            </div>
          </div>
        </section>
      <?php endwhile; ?>
    <?php endif; ?>
    <?php if(have_rows('client') ): ?>
      <?php while(have_rows('client')): ?> <?php the_row() ?>
      <section>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h2 class="title-md"><?php echo e(get_sub_field('client_title')); ?></h2>
            </div>
            <div class="col-md-6">
              <p class="text-default"><?php echo e(get_sub_field('client_description')); ?> <a href="<?php echo e(get_sub_field('get_in_touch_link')); ?>" class="text-link">Get in touch</a></p>
            </div>
          </div>
          <div class="clients-list bg-gray">
            <div class="row">
              <?php if(have_rows('client_list')): ?>
                <?php while(have_rows('client_list')): ?> <?php the_row() ?>
                  <div class="col-md-3 col-3"> 
                    <div class="client-item">
                      <img src="<?php echo e(get_sub_field('client_item')); ?>" alt="">
                    </div>
                  </div>
                <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </section>
      <?php endwhile; ?>
    <?php endif; ?>
    <section class="section-testimonial">
      <div class="container-fluid">
        <div class="bg-blue">
          <p class="text-blue">Trusted by others</p>
          <!-- Set up your HTML -->
          <div class="event-slider owl-carousel owl-theme">
            <?php if(have_rows('testimonials')): ?>
              <?php while(have_rows('testimonials')): ?> <?php the_row() ?>
                <div class="item" data-dot="<button class='slider-pagination'><?php echo e(get_row_index()); ?></button>">
                  <p class="testimonial-msg">
                    <?php echo e(get_sub_field('testimonial_description')); ?>

                  </p>
                  <p class="testimonial-author">
                    <?php echo e(get_sub_field('testimonial_author')); ?>

                  </p>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>