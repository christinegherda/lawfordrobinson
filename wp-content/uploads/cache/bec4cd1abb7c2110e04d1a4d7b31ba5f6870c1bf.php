
<?php
 $is_subpage = '';
 if(! is_front_page()) {
  $is_subpage = 'header-subpage';
 }
?>

<header class="main-header <?php echo e($is_subpage); ?>">
  <nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="<?php echo e(home_url('/')); ?>">
      <img src="<?php echo e(get_template_directory_uri().'/assets/images/brand.png'); ?>" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fa fa-bars"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <?php if(has_nav_menu('primary_navigation')): ?>
          <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => 'ul', 'menu_class' => 'navbar-nav ml-auto']); ?>

      <?php endif; ?>
    </div>
  </nav>
</header>