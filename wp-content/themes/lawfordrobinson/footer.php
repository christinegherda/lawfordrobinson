<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<footer>
  <div class="container-fluid">
    <div class="footer-upper">
      <div class="row">
        <div class="col-md-4">
          <div class="footer-brand">
			<?php if ( has_custom_logo() ) : ?>
				<?php the_custom_logo(); ?>
			<?php endif; ?>
            
          </div>
        </div>
        <div class="col-md-8 text-right">
        	<?php if (has_nav_menu('footer')): ?>
				<?php wp_nav_menu(['theme_location' => 'footer', 'container' => 'ul', 'menu_class' => 'footer-menu']) ?>
			<?php endif ?>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="row">
        <div class="col-md-6">
          <?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
        </div>
        <div class="col-md-6 text-right">
          <p>Site by Hello Again</p>
        </div>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
