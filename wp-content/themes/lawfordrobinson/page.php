<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>
	<main>
		
		<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>

		<div class="main-content">
			<div class="container-fluid">
				<h1><?=the_title(); ?></h1>
				<p>Coming soon..</p>
			</div>
		</div>
	</main>

<?php
get_footer();
