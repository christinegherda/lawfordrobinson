var $ = jQuery;

$(document).ready(function(){
  $(".event-slider").owlCarousel({
    // autoplay: true,
    // autoplayTimeout: 4000,
    dots: true,
    dotsData:true,
    loop: true,
    margin: 30,
    nav: true,
    items: 1,
    navText: ["<span class='icon icon-prev'>","<span class='icon icon-next'>"]

  });
});