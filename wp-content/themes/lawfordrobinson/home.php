<?php
/**
 * Template Name: Home Page Template
 *
 * 
 */

get_header();
?>
	<?php if (have_rows('banner') ): ?>
		<?php while ( have_rows('banner') ): the_row(); ?>
	        <section class="section-banner" style="background-image: url('<?=get_sub_field('banner_image'); ?>')">
	          <!-- @include('partials.header') -->
	          <?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
	          <div class="banner-content">
	            <div class="container">
	              <div class="banner-text">
	                <h2><?=get_sub_field('banner_title'); ?></h2>
	                <p><?=get_sub_field('banner_subtitle'); ?></p>
	                <a type="button" href="<?=get_sub_field('banner_contact_link'); ?>" class="btn btn-white">Contact Us</a>
	              </div>
	              <div class="scroll-down">
	                <p>Scroll Down</p>
	                <span class="icon-scrolldown"></span>
	              </div>
	            </div>
	          </div>
	        </section>
		<?php endwhile; ?>
	<?php endif ?>

	<?php if (have_rows('expertise') ): ?>
		<?php while ( have_rows('expertise') ): the_row(); ?>
        <section class="section-expertise">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <h2 class="title-md"><?=get_sub_field('expertise_title'); ?></h2>
                <p class="text-default">
                  <?=get_sub_field('expertise_description'); ?>
                </p>
                <a href="<?=get_sub_field('learn_more_link'); ?>" class="text-link">Learn More</a>
              </div>
              <div class="col-md-6">

              <div class="venn-container">
              	<?php if (have_rows('venn_diagram')): ?>
              		<?php while ( have_rows('venn_diagram') ): the_row(); ?>
						<?php 
	                      $circleWrapper = "";
	                      $circleText = "";
	                      if (get_row_index() == 1) {
	                        $circleWrapper = "venncirctop";
	                        $circleText = "venntxttop";
	                      } elseif (get_row_index() == 2) {
	                        $circleWrapper = "venncirclft";
	                        $circleText = "venntxtlft";
	                      } elseif (get_row_index() == 3) {
	                        $circleWrapper = "venncircrt";
	                        $circleText = "venntxtrt";
	                      } else {
	                        $circleWrapper = "venncircbtm";
	                        $circleText = "venntxtbtm";
	                      }
						?>
	                    <div class="<?=$circleWrapper; ?>">
	                    	<a class="<?=$circleText; ?>" href="javascript:void(0)"><?=get_sub_field('venn_item', true); ?></a>
	                    </div>
					<?php endwhile; ?>
              	<?php endif ?>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>

              </div>


              </div>
            </div>
          </div>
        </section>
		<?php endwhile; ?>
	<?php endif ?>

	<?php if (have_rows('client') ): ?>
		<?php while ( have_rows('client') ): the_row(); ?>
	      <section>
	        <div class="container">
	          <div class="row">
	            <div class="col-md-6">
	              <h2 class="title-md"><?=get_sub_field('client_title'); ?></h2>
	            </div>
	            <div class="col-md-6">
	              <p class="text-default"><?=get_sub_field('client_description'); ?> <a href="<?=get_sub_field('get_in_touch_link'); ?>" class="text-link">Get in touch</a></p>
	            </div>
	          </div>
	          <div class="clients-list bg-gray">
	            <div class="row">

				<?php if (have_rows('client_list') ): ?>
					<?php while ( have_rows('client_list') ): the_row(); ?>
	                  <div class="col-md-3 col-3"> 
	                    <div class="client-item">
	                      <img src="<?=get_sub_field('client_item'); ?>" alt="">
	                    </div>
	                  </div>
					<?php endwhile; ?>
				<?php endif ?>

	            </div>
	          </div>
	        </div>
	      </section>
		<?php endwhile; ?>
	<?php endif ?>
	

	<?php if (have_rows('testimonials') ): ?>
	    <section class="section-testimonial">
	      <div class="container-fluid">
	        <div class="bg-blue">
	          <p class="text-blue">Trusted by others</p>
	          <!-- Set up your HTML -->
	          <div class="event-slider owl-carousel owl-theme">
	              <?php while ( have_rows('testimonials') ): the_row(); 
	              		$index = get_row_index();
	              		$num = sprintf("%02d", $index);
	              	?>
	                <div class="item" data-dot="<button class='slider-pagination'><?=$num; ?></button>">
	                  <p class="testimonial-msg">
	                    <?=get_sub_field('testimonial_description') ?>
	                  </p>
	                  <p class="testimonial-author">
	                    <?=get_sub_field('testimonial_author') ?>
	                  </p>
	                </div>
	              <?php endwhile; ?>
	          </div>
	        </div>
	      </div>
	    </section>
	<?php endif ?>

<?php get_footer(); ?>