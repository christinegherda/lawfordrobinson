<?php
/**
 * Displays the footer widget area
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
    <p class="copyright"><?php dynamic_sidebar( 'sidebar-1' ); ?></p>
<?php endif; ?>
