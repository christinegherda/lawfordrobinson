<?php
/**
 * Displays the post header
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

 $is_subpage = '';
 if(! is_front_page()) {
  $is_subpage = 'header-subpage';
 }
?> 
<header class="main-header <?=$is_subpage; ?>">
  <nav class="navbar navbar-expand-lg">
	<?php if ( has_custom_logo() ) : ?>
		<div class="site-logo">
			<?php the_custom_logo(); ?>
		</div>
	<?php endif; ?>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fa fa-bars"></span>
    </button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<?php
		wp_nav_menu(
			array(
				'theme_location' => '',
				'menu' => 'header',
				'menu_class'     => 'navbar-nav ml-auto',
				'container'     => 'ul',
			)
		);
		?>
	</div>
  </nav>
</header>
